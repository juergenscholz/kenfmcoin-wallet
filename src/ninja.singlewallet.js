(function (wallets, qrCode) {
	var single = wallets.singlewallet = {
		isOpen: function () {
			return (document.getElementById("singlewallet").className.indexOf("selected") != -1);
		},

		open: function () {
			if (document.getElementById("kenaddress").innerHTML == "") {
				single.generateNewAddressAndKey();
			}
			document.getElementById("singlearea").style.display = "block";
		},

		close: function () {
			document.getElementById("singlearea").style.display = "none";
		},

		// generate kenfmcoin address and private key and update information in the HTML
		generateNewAddressAndKey: function () {
			try {
				var key = new KenFMcoin.ECKey(false);
				key.setCompressed(true);
				var kenfmcoinAddress = key.getKenFMcoinAddress();
				var privateKeyWif = key.getKenFMcoinWalletImportFormat();
				document.getElementById("kenaddress").innerHTML = kenfmcoinAddress;
				document.getElementById("kenprivwif").innerHTML = privateKeyWif;
				var keyValuePair = {
					"qrcode_public": kenfmcoinAddress,
					"qrcode_private": privateKeyWif
				};
				qrCode.showQrCode(keyValuePair, 4);
			}
			catch (e) {
				// browser does not have sufficient JavaScript support to generate a kenfmcoin address
				alert(e);
				document.getElementById("kenaddress").innerHTML = "error";
				document.getElementById("kenprivwif").innerHTML = "error";
				document.getElementById("qrcode_public").innerHTML = "";
				document.getElementById("qrcode_private").innerHTML = "";
			}
		}
	};
})(ninja.wallets, ninja.qrCode);
